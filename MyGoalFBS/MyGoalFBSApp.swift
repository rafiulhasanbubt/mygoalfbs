//
//  MyGoalFBSApp.swift
//  MyGoalFBS
//
//  Created by rafiul hasan on 27/11/21.
//

import SwiftUI
import Firebase

@main
struct MyGoalFBSApp: App {
    
    init() {
        FirebaseApp.configure()
    }
    
    var body: some Scene {
        WindowGroup {
            //ContentView()
            //ContentView(repo: MockGoalRepository())
            ContentView(repo: FirebaseGoalRepository() as! GoalRepositoryProtocol)
        }
    }
}
