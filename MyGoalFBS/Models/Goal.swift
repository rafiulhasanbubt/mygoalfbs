//
//  Goal.swift
//  MyGoalFBS
//
//  Created by rafiul hasan on 27/11/21.
//

import Foundation

struct Goal: Codable {
    var id: String?
    var name: String
    var dueOn: Date
    var color: String
    var icon: String
    var items: [String]
}

extension Goal {
    static func sampleGoals() -> [Goal] {
        return [
            Goal(id: UUID().uuidString, name: "Work", dueOn: Date(), color: "#0984e3", icon: "💼", items: ["Work on youtube", "Publish youtube", "Upload source code"]),
            Goal(id: UUID().uuidString, name: "Personal", dueOn: Date(), color: "e17055", icon: "🔐", items: ["Work on youtube", "Publish youtube", "Upload source code"]),
            Goal(id: UUID().uuidString, name: "Fitness", dueOn: Date(), color: "#6c5ce7", icon: "🏃‍♂️", items: ["Work on youtube", "Publish youtube", "Upload source code"]),
            Goal(id: UUID().uuidString, name: "Traval", dueOn: Date(), color: "#00b894", icon: "🌊", items: ["Work on youtube", "Publish youtube", "Upload source code"]),
            Goal(id: UUID().uuidString, name: "Gigs", dueOn: Date(), color: "#fd79a8", icon: "✨", items: ["Work on youtube", "Publish youtube", "Upload source code"]),
            Goal(id: UUID().uuidString, name: "Work", dueOn: Date(), color: "#2d3436", icon: "👔", items: ["Work on youtube", "Publish youtube", "Upload source code"])
        ]
    }
}
