//
//  GoalViewModel.swift
//  MyGoalFBS
//
//  Created by rafiul hasan on 28/11/21.
//

import Foundation
import UIKit
import SwiftUI

struct GoalViewModel {
    var goal : Goal
    
    var id: String {
        goal.id ?? ""
    }
    
    var name: String {
        goal.name
    }
    
    var dueOnDate: Date {
        goal.dueOn
    }
    
    var dueOn: String {
        goal.dueOn.toRelativeDate()
    }
    
    var color: Color {
        Color(UIColor(hexString: goal.color))
    }
    
    var icon: String {
        goal.icon
    }
    
    var items: [String] {
        get {
            return goal.items
        }
        
        set(newValue) {
            goal.items = newValue
        }
    }
}
