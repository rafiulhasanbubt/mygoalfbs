//
//  GoalListViewModel.swift
//  MyGoalFBS
//
//  Created by rafiul hasan on 28/11/21.
//

import Foundation

class GoalListViewModel: ObservableObject {
    private var repo: GoalRepositoryProtocol
    @Published var goals = [GoalViewModel]()
    
    init(repo: GoalRepositoryProtocol) {
        self.repo = repo
    }
    
    func getAllGoals() {
        repo.getAll { result in
            switch result {
            case .success(let fetchedgoal):
                if let fetchedgoal = fetchedgoal {
                    DispatchQueue.main.async {
                        self.goals = fetchedgoal.map(GoalViewModel.init)
                    }
                }
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
    
    func deleteGoal(goalId: String) {
        repo.deleteGoal(goalId: goalId) { result in
            switch result {
            case .success(let getgoal):
                if getgoal {
                    self.getAllGoals()
                }
                
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
    
}
