//
//  AddGoalViewModel.swift
//  MyGoalFBS
//
//  Created by rafiul hasan on 29/11/21.
//

import Foundation

class AddGoalViewModel: ObservableObject {
    private var repo: GoalRepositoryProtocol
    var name: String = ""
    var dueOn = Date()
    var color: String = ""
    var icon: String = "✨"
    var goal: String = ""
    @Published var saved: Bool = false
    @Published var items: [String] = []
    
    init(repo: GoalRepositoryProtocol) {
        self.repo = repo
    }
    
    func add() {
        let goal = Goal(name: name, dueOn: dueOn, color: color, icon: icon, items: items)
        
        repo.add(goal: goal) { result in
            switch result {
            case .success(let savedGoal):
                DispatchQueue.main.async {
                    self.saved = savedGoal == nil ? false : true
                }
                
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
}
