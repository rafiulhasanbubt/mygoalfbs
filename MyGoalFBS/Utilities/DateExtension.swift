//
//  DateExtension.swift
//  MyGoalFBS
//
//  Created by rafiul hasan on 28/11/21.
//

import Foundation

extension Date {
    func toRelativeDate() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .short
        dateFormatter.doesRelativeDateFormatting = true
        return dateFormatter.string(from: self)
    }
}
