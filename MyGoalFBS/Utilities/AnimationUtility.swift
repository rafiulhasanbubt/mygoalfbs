//
//  AnimationUtility.swift
//  MyGoalFBS
//
//  Created by rafiul hasan on 28/11/21.
//

import Foundation
import SwiftUI

extension Animation {
    func `repeat`(while expression: Bool, autoreverses: Bool = true) -> Animation {
        if expression {
            return self.repeatForever(autoreverses: autoreverses)
        } else {
            return self
        }
    }
}
