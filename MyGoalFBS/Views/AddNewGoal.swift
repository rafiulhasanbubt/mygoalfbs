//
//  AddNewGoal.swift
//  MyGoalFBS
//
//  Created by rafiul hasan on 29/11/21.
//

import SwiftUI

struct AddNewGoal: View {
    @StateObject private var addGoalVM: AddGoalViewModel
    @Environment(\.presentationMode) var presentationMode
    init(repo: GoalRepositoryProtocol) {
        _addGoalVM = StateObject<AddGoalViewModel>.init(wrappedValue: AddGoalViewModel(repo: repo))
    }
    
    var body: some View {
        NavigationView {
            VStack(spacing: 0) {
                Form {
                    Section {
                        TextField("Goal name", text: $addGoalVM.name)
                        DatePicker(selection: $addGoalVM.dueOn, in: Date()..., displayedComponents: .date) {
                            Text("Select goal due date")
                        }.id(addGoalVM.dueOn)
                        
                        ColorPicker("Select goal color", selection: $addGoalVM.color)
                        
                        EmojiTextField(text: $addGoalVM.icon, placeholder: "Enter emoji icon")
                    }
                }
                .frame(maxHeight: 200)
            }
        }
    }
}

struct AddNewGoal_Previews: PreviewProvider {
    static var previews: some View {
        AddNewGoal(repo: MockGoalRepository())
    }
}
