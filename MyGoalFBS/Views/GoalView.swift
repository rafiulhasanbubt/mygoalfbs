//
//  GoalView.swift
//  MyGoalFBS
//
//  Created by rafiul hasan on 29/11/21.
//

import SwiftUI

struct GoalView: View {
    let goalViewModel: GoalViewModel
    
    var body: some View {
        VStack(alignment: .leading) {
            Text(goalViewModel.name)
                .font(.largeTitle)
                .multilineTextAlignment(.leading)
            
            Text(goalViewModel.dueOn.lowercased())
            
            HStack(alignment: .bottom) {
                Text("\(goalViewModel.items.count) items")
                    .padding(.top, 20)
                Spacer()
                Text(goalViewModel.icon)
                    .font(.largeTitle)
            }
        }
    }
}

struct GoalView_Previews: PreviewProvider {
    static var previews: some View {
        GoalView(goalViewModel: GoalViewModel(goal: Goal.sampleGoals()[1]))
    }
}
